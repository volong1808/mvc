<?php
require_once "config/database.php";

class Model extends Database
{
    protected $table = '';

    public function getAll($select = "*", $condition = null)
    {
        $data = [];
        $query = mysqli_query($this->conn, "SELECT ". $select . " FROM " .$this->table . " " .$condition);
        if(!empty($this->table)){
            while($row = mysqli_fetch_assoc($query)){
                $data[] = $row;
            }
        }
        return $data;
    }

    public function getFirst($getAll){
        if(!empty($getAll)){
            return $getAll[0];
        }else return "Data Not Found";
    }

    public function getById($id){
        $data = $this->getFirst($this->getAll('*', "WHERE id='".$id."'"));
        if($data != null){
            return $data;
        }else return "Data Not Found";
    }

    public function delete($condition){
        mysqli_query($this->conn, "DELETE FROM " .$this->table . " ". $condition);
    }


    public function update($columns_values, $condition){
        mysqli_query($this->conn, "UPDATE " .$this->table . " SET ". $columns_values . $condition);
    }

    public function innerJoin($listColumns = '*', $tableSecond, $onWhere){
        $data =  mysqli_query($this->conn, "SELECT ". $listColumns . " FROM products INNER JOIN " . $tableSecond . " ON ". $onWhere);
        if($data != null){
            return $data;
        }else return "Data Not Found";
    }
}
<!DOCTYPE html>
<html>
<head>
    <title>Edit Product</title>
    <link rel="stylesheet" href="public/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="public/css/colors.min.css">
    <link rel="stylesheet" href="public/css/main.css">
    <script src="public/js/jquery-3.4.1.min.js"></script>
    <script src="public/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="admin__container">
    <div class="container">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="?controller=product&method=update" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_edit" value="<?php echo $id; ?>">
                    <div class="form__item">
                        <label>Tên Sản Phẩm: </label><input type="text" name="name_edit" class="form-control"
                                                            value="<?php echo $product['name']; ?>">
                    </div>
                    <div class="form__item">
                        <label>Giá: </label><input type="number" name="price_edit" class="form-control"
                                                             value="<?php echo $product['price']; ?>">
                    </div>
                    <div class="form__item">
                        <label>Mô Tả: </label><textarea class="form-control" name="description_edit"
                                                                      rows="5"
                                                                      cols="70"><?php echo $product['description']; ?></textarea>
                    </div>
                    <div class="form__item">
                        <div class="form__item">
                            <label>Hình Ảnh: </label>
                            <img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" height="80">
                            </div>
                        <input class="form-control" type="file" name="image_edit">
                    </div>
                    <div class="form__item">
                        <label>Danh mục: </label>
                        <select name="category_edit">
                            <option value="1">Smart Phone</option>
                            <option value="2">Laptop</option>
                            <option value="3">Watch</option>
                        </select>
                    </div>
                    <div class="form__item">
                        <input type="submit" name="submit_edit" value="SỬA" class="btn btn-primary">
                    </div>
                </form>
                <?php if (isset($_COOKIE["success"])) {
                    ?>
                    <div class="alert alert-success">
                        <strong> <?php echo $_COOKIE["success"]; ?></strong>
                    </div>
                <?php } ?>
                <?php if (isset($_COOKIE["error"])) {
                    ?>
                    <div class="alert alert-danger">
                        <strong> <?php echo $_COOKIE["error"]; ?></strong>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <body>
</html>
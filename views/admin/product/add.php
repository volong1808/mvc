<!DOCTYPE html>
<html>
<head>
    <title>Add Product</title>
    <link rel="stylesheet" href="public/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="public/css/colors.min.css">
    <link rel="stylesheet" href="public/css/main.css">
    <script src="public/js/jquery-3.4.1.min.js"></script>
    <script src="public/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="admin__container">
    <div class="container">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="?controller=product&method=create" method="post" enctype="multipart/form-data">
                    <div class="form__item">
                        <label>Tên Sản Phẩm: </label><input required type="text" name="name" class="form-control">
                    </div>
                    <div class="form__item">
                        <label>Giá: </label><input required type="number" name="price" class="form-control">
                    </div>
                    <div class="form__item">
                        <label>Mô Tả: </label><textarea required class="form-control" name="description"
                                                                      rows="5"
                                                                      cols="70"></textarea>
                    </div>
                    <div class="form__item">
                        <label>Hình Ảnh: </label><input required class="form-control" type="file" name="image">
                    </div>
                    <div class="form__item">
                        <label>Danh mục: </label>
                        <select name="category">
                            <option value="1">Smart Phone</option>
                            <option value="2">Laptop</option>
                            <option value="3">Watch</option>
                        </select>
                    </div>
                    <div class="form__item">
                        <input type="submit" name="submit" value="THÊM" class="btn btn-success">
                        <a class="btn btn-primary" href="admin.php">Trang Quản Lý</a>
                    </div>
                </form>
                <?php if (isset($_COOKIE["success"])) {
                    ?>
                    <div class="alert alert-success">
                        <strong> <?php echo $_COOKIE["success"]; ?></strong>
                    </div>
                <?php } ?>
                <?php if (isset($_COOKIE["error"])) {
                    ?>
                    <div class="alert alert-danger">
                        <strong> <?php echo $_COOKIE["error"]; ?></strong>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
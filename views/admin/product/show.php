<!DOCTYPE html>
<html>
<head>
    <title>List Product</title>
    <link rel="stylesheet" href="public/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="public/css/main.css">
</head>
<body>
<table class="table table-bordered table-framed">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Price</th>
        <th>Description</th>
        <th>Image</th>
    </tr>
    <?php
    foreach ($products as $product){ ?>
        <tr>
            <td><?php echo $product['id']; ?></td>
            <td><?php echo $product['name']; ?></td>
            <td><?php echo $product['price']; ?></td>
            <td><?php echo $product['description']; ?></td>
            <td><?php echo $product['image']; ?></td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
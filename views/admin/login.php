<!DOCTYPE html>
<html>
<head>
    <title>Login Admin</title>
    <link rel="stylesheet" href="public/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="public/css/main.css">
</head>
<body>
<div class="container home__container">
    <div class="col-md-3">
        <form action="admin_login.php" method="post">
            <div class="alert alert-info logo">
                <img src="public/image/logo.png" alt="logo">
            </div>

            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="email">Tài khoản:</label>
                        <input type="text" class="form-control" name="username" placeholder="Nhập tên đăng nhập...">
                    </div>

                    <div class="form-group">
                        <label for="pwd">Mật khẩu:</label>
                        <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu..."
                               required>
                    </div>

                    <div class="text-center"><button type="submit" class="btn btn-primary" name="login">Đăng nhập</button></div>
                </div>
            </div>
            <?php
            if (isset($_COOKIE["error"])) {
                ?>
                <div class="alert alert-danger">
                    <strong> <?php echo $_COOKIE["error"]; ?></strong>
                </div>
            <?php } ?>

        </form>
    </div>
</div>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <title>Trang Quản Lý</title>
    <link rel="stylesheet" href="public/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="public/css/colors.min.css">
    <link rel="stylesheet" href="public/css/main.css">
    <script src="public/js/jquery-3.4.1.min.js"></script>
    <script src="public/css/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
<div class="admin__container">
    <div class="container">

        <div class="header-wrap">
                <h1 class="header__text">Trang Quản Lý Sản Phẩm</h1>
            <div class="header__user">
                <img src="<?php echo $user['avatar']; ?>" alt="avatar" height="30">
                <h4><?php echo $user['full_name']; ?></h4>
                <a class="btn btn-danger" href="javascript:void(0)" data-toggle="modal" data-target="#logoutModal">Đăng Xuất</a>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="bootbox modal fade bootbox-confirm in" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×
                        </button>
                        <h5 class="modal-title text-capitalize">Xác nhận</h5>
                    </div>
                    <div class="modal-body">
                        <h6 class="bootbox-body">Bạn có muốn đăng xuất khỏi trang web không?</h6>
                    </div>
                    <div class="modal-footer">
                        <button data-bb-handler="cancel" type="button" data-dismiss="modal" class="btn btn-dark legitRipple">Huỷ</button>
                        <a href="?controller=logout" data-bb-handler="confirm" type="button" class="btn btn-danger legitRipple">Đăng Xuất</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="list_product">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <a class="btn btn-success" href="?controller=product&method=add">Thêm Sản Phẩm</a>
                    <?php if ($errors){
                        echo "<div class='alert alert-danger'><strong>". $errors . "</strong></div>";
                    }else{ ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-framed">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hình Ảnh</th>
                                    <th width="130">Tên Sản Phẩm</th>
                                    <th>Giá</th>
                                    <th>Mô tả</th>
                                    <th width="120">Danh Mục</th>
                                    <th width="130"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $stt = 1;
                                foreach ($products as $product) { ?>
                                    <tr>
                                        <td><?php echo $stt++; ?></td>
                                        <td class="text-center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" height="50"> </td>
                                        <td><?php echo $product['name']; ?></td>
                                        <td><?php echo $product['price']; ?></td>
                                        <td><?php echo $product['description']; ?></td>
                                        <td><?php echo $product['cate_name']; ?></td>
                                        <td class="text-center">
                                            <a href="?controller=product&method=edit&id=<?php echo $product['id']?>" class="btn btn-sm btn-icon btn-primary dis">
                                                Sửa
                                            </a>
                                            <button type="button" class="btn btn-sm btn-icon btn-danger" data-toggle="modal"
                                                    data-target="#delete-modal" data-id="<?php echo $product['id']; ?>">
                                               Xóa
                                            </button>

                                            <!-- Delete modal -->
                                            <div id="delete-modal" class="modal fade" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg-danger">
                                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                                            <h4 class="lign-content-center">Xóa Sản Phẩm</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            Sản Phẩm đã xóa sẽ không thể khôi phục lại, bạn có muốn tiếp tục không?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <form action="?controller=product&method=delete" method="post">
                                                                <input type="hidden" name="id">
                                                                <button type="button" class="btn btn-link" data-dismiss="modal">Hủy</button>
                                                                <button type="submit" class="btn btn-danger bg-danger">Đồng ý</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                    <?php if (isset($_COOKIE["success"])) {
                        ?>
                        <div class="alert alert-success">
                            <strong> <?php echo $_COOKIE["success"]; ?></strong>
                        </div>
                    <?php } ?>
                    <?php if (isset($_COOKIE["error"])) {
                        ?>
                        <div class="alert alert-danger">
                            <strong> <?php echo $_COOKIE["error"]; ?></strong>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#delete-modal').on('show.bs.modal', function (e) {
        var productId = $(e.relatedTarget).attr('data-id');
        $(e.currentTarget).find('input[name="id"]').val(productId);
    })
</script>
</body>
</html>


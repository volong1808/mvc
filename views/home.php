<!DOCTYPE html>
<html>
<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="public/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="public/css/main.css">
</head>
<body>
<div class="container-fluid home__container">
    <div class="home__content">
        <div class="home__title-wrap">
            <p class="home__title">Comming Soon ...</p>
        </div>
        <a href="admin.php" class="home__btn-admin">Admin Page</a>
    </div>
</div>
</body>
</html>
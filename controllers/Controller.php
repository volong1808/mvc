<?php


class Controller
{

    public function view($views, $data)
    {
        extract($data);
        include "{$views}";
    }

    public function execute($method, $object, $param = null)
    {
        if (empty($method)) {
            $method = 'index';
        }
        return $object->{$method}();
    }

}
<?php
require_once "controllers/Controller.php";
require_once "controllers/ProductController.php";
require_once "models/UserModel.php";
require_once "models/ProductModel.php";

class AdminController extends Controller
{
    public function __construct()
    {
        $this->user_model = new UserModel();
        $this->product_model = new ProductModel();
    }

    public function index()
    {
        include "views/admin/login.php";
    }


    public function dashboard(){
        $user = $_SESSION['user'];
        if(empty($this->product_model->getAll())){
            $data = [
                'user' => $user,
                'errors' => 'Không có sản phẩm nào!!!'
            ];
        }else {
            $join_products = $this->product_model->joinProduct();
            $data = [
                'user' => $user,
                'products' => $join_products,
                'errors' => null,
            ];
        }
        $this->view("views/admin/dashboard.php", $data);
    }

    public function logout(){
        unset($_SESSION['user']);
        $_SESSION['logout'] = "logout";
        header("Location:admin.php");
        exit();
    }

}
<?php
require_once "controllers/Controller.php";
require_once "models/ProductModel.php";

class ProductController extends Controller
{

    public function __construct()
    {
        $this->model = new ProductModel();
    }

    public function index()
    {
        return $this->showAll();
    }

    public function showAll()
    {
        $products = $this->model->getAll();
        $data = [
            'products' => $products,
        ];
        $this->view("views/admin/product/show.php", $data);
    }

    public function delete()
    {
        $id = $_POST['id'];
        $this->model->deleteProduct($id);
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        setcookie("success", "Chúc mừng bạn đã xóa thành công sản phẩm!", time() + 1, "/", "", 0);
    }

    public function add()
    {
        $this->view("views/admin/product/add.php", []);
    }

    public function create()
    {
        if (isset($_POST["submit"])) {
            $allowed_image_extension = array(
                "png",
                "jpg",
                "jpeg"
            );
            $file_extension = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
            if (in_array($file_extension, $allowed_image_extension)) {
                $name = $_POST['name'];
                $description = $_POST['description'];
                $price = $_POST['price'];
                $cate_id = $_POST['category'];
                //lấy tên của file:
                $file_name = $_FILES['image']["name"];
                //lấy đường dẫn tạm lưu nội dung file:
                $file_tmp = $_FILES['image']['tmp_name'];
                //tạo đường dẫn lưu file:
                $image = "public/upload/" . $file_name;
                //upload nội dung file vào đường dẫn vừa tạo:
                move_uploaded_file($file_tmp, $image);
                //tạo kết nối đến database
                //thực hiện insert vào bảng đường dẫn file vừa upload:

                $data = array(
                    'name' => $name,
                    'price' => $price,
                    'description' => $description,
                    'image' => $image,
                    'cate_id' => $cate_id,
                );
                $this->model->addProduct($data);

                setcookie("success", "Chúc mừng bạn đã thêm thành công sản phẩm!", time() + 1, "/", "", 0);
                header("Location:?controller=product&method=add");
                exit();

            } else {
                setcookie("error", "Kiểm tra lại thông tin", time() + 1, "/", "", 0);
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                exit();
            }


        }

    }

    public function edit()
    {
        $id = $_GET['id'];
        $product = $this->model->getById($id);
        $data = [
            'id' => $id,
            'product' => $product,
        ];
        $this->view("views/admin/product/edit.php", $data);
    }

    public function update()
    {
        if (isset($_POST["submit_edit"])) {
            $id = $_POST['id_edit'];
            $product = $this->model->getById($id);


            if ($_POST['name_edit'] == "") {
                $name = $product['name'];
            } else {
                $name = $_POST['name_edit'];
            }

            if ($_POST['price_edit'] == "") {
                $price = $product['price'];
            } else {
                $price = $_POST['price_edit'];
            }

            if ($_POST['description_edit'] == "") {
                $description = $product['description'];
            } else {
                $description = $_POST['description_edit'];
            }

            $allowed_image_extension = array(
                "png",
                "jpg",
                "jpeg"
            );
            $image = $product['image'];
            $cate_id = $_POST['category_edit'];

            if (isset($_FILES["image_edit"]) && $_FILES["image_edit"]["name"] != null) {
                $file_extension = pathinfo($_FILES["image_edit"]["name"], PATHINFO_EXTENSION);
                if (in_array($file_extension, $allowed_image_extension)) {

                    $file_name = $_FILES['image_edit']["name"];
                    $file_tmp = $_FILES['image_edit']['tmp_name'];
                    $file_image = "public/upload/" . $file_name;

                    if ($file_image != $image) {
                        $image = $file_image;
                        move_uploaded_file($file_tmp, $file_image);

                        $data = array(
                            'name' => $name,
                            'price' => $price,
                            'description' => $description,
                            'image' => $image,
                            'cate_id' => $cate_id,
                        );
                        $this->model->updateProduct($data, $id);

                        setcookie("success", "Chúc mừng bạn đã sửa thành công sản phẩm!", time() + 1, "/", "", 0);
                        header("Location:?controller=dashboard");
                        exit();

                    } else {
                        setcookie("error", "Hình ảnh đã tồn tại!", time() + 1, "/", "", 0);
                        header('Location: ' . $_SERVER['HTTP_REFERER']);
                        exit();
                    };
                } else {
                    setcookie("error", "Tập tin phải là hình ảnh!", time() + 1, "/", "", 0);
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                    exit();
                }
            } else {

                $data = array(
                    'name' => $name,
                    'price' => $price,
                    'description' => $description,
                    'image' => $image,
                    'cate_id' => $cate_id,
                );

                $this->model->updateProduct($data, $id);

                setcookie("success", "Chúc mừng bạn đã sửa thành công sản phẩm!", time() + 1, "/", "", 0);
                header("Location:?controller=dashboard");
                exit();
            }

        }
    }

}
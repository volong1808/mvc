<?php
session_start();
require_once "controllers/AdminController.php";

    if( isset($_SESSION['logout'])){
        $admin = new AdminController();
        $admin->index();
        $_SESSION['logout'] = "logout";
    }
    else {
        if (isset($_GET['controller'])) {
            if (!isset($_SESSION['logout'])) {
                $controller = $_GET['controller'];
                $method = '';
                if (!empty($_GET['method'])) {
                    $method = $_GET['method'];
                }
                $param = null;
                if (!empty($_GET['id'])) {
                    $param = $_GET['id'];
                }
                switch ($controller) {
                    case 'product':
                        require_once "controllers/ProductController.php";
                        $product = new ProductController();
                        $product->execute($method, $product, $param);
                        break;
                    case 'dashboard':
                        $admin = new AdminController();
                        $admin->dashboard();
                        break;
                    case 'logout':
                        $admin = new AdminController();
                        $admin->logout();
                        break;
                    default:
                        header("Location:admin.php?controller=dashboard");
                        break;
                }
            }
        }else{
            $admin = new AdminController();
            $admin->dashboard();
        }
    }
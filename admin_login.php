<?php
session_start();

    if(isset($_POST["login"])) {
        $username = $_POST["username"];
        $password = md5($_POST["password"]);
        require_once "models/UserModel.php";
        $user_model = new UserModel();
        $user = $user_model->checkUser($username, $password);
        if ($user){
            $_SESSION['user'] = $user_model->getFirst($user);
            unset($_SESSION['logout']);
            header("Location:admin.php?controller=dashboard");
            exit();
        }else{
            setcookie("error","Tên đăng nhập hoặc mật khẩu không đúng!", time()+1,"/","", 0);
            header("Location:admin.php");
            exit();
        }
    }
